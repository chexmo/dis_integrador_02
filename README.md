# Trabajo integrador

Este proyecto está realizado para ser entregado como una práctica para el ISFT194

[Documento](https://docs.google.com/document/d/1QZRZjM9t2Pd_ouoNOJvuiFEljnNomqLGXRJ7NFPkR5c/edit?usp=sharing)

- [Trabajo integrador](#trabajo-integrador)
  - [Proceso](#proceso)
  - [Modelo actual](#modelo-actual)
  - [Observaciones](#observaciones)
  - [Por hacer o resolver](#por-hacer-o-resolver)
  - [Hecho o resuelto](#hecho-o-resuelto)
  - [Patrones de diseño](#patrones-de-diseño)
    - [Patron observer](#patron-observer)
    - [Patron factory method](#patron-factory-method)
    - [Patron template](#patron-template)
    - [Patron strategy](#patron-strategy)
    - [Patrones en la API](#patrones-en-la-api)

## Proceso

En realidad este es el segundo intento del mismo proyecto. La razon por la cual el proyecto anterior fue dejado de lado es que como podíamos elegir las tecnologías que componen el stack, he elegido de manera un poco ambiciosa algunas tecnologías que personalmente me interesan, pero que tal vez exedían la necesidad de este proyecto, y dado que en este momento faltan pocos días para la entrega decidí que no valía la pena "perder tiempo" lidiando con ellas.

Por lo tanto, la medida tomada fue la de imitar el mismo modelo en un proyecto java y continuar con java de aquí en adelante. Una vez pasada la entrega probablemente retome el proyecto primitivo para intentar resolver algunas cuestiones que me trabaron, para completar mi desafío y para ganar esa experiencia.

## Modelo actual

El modelo es el resultado de unas prácticas realizadas en EDI, que al momento de iniciar la implementación de este proyecto ya ha pasado por varios momentos de analisis y diseño. De hecho, el comienzo de la implementación de este proyecto y este repositorio comienza con la version v6, se irá actualizando para que incluya algunas observaciones vistas al momento de poner manos en el código.

```plantuml

@startuml Alarmas

title Sistema de Seguridad v7
' --------------------------------------------- RELACIONES
Observador <|.. SistemaDeSeguridad
Observado  <|.. Dispositivo
Observador  .>  Observado   : Observa 

Dispositivo <|-- Rociador
Dispositivo <|-- Alarma
Dispositivo *->  Sensor : 1:1
Dispositivo  .>  Sensor : Dependency\nIncjection 

SistemaDeSeguridad --> Modem       : 1:1
SistemaDeSeguridad  -> Dispositivo : 1:N


' --------------------------------------------- INTERFACES
interface Observador{
    + serNotificado()
}

interface Observado{
    + addObservador()
    + remObservador()
    + notificar()
}

' --------------------------------------------- CLASES
class Modem{
    - puerto
    + llamarA(numero, mensaje)
}

class Dispositivo{
    - identificador : String
    - umbralDeActivacion : int
    - sensor : Sensor
    + actualizar()
    + activarse()
}

class Sensor{
    - ultimaMedicion : int
    + medir()
}

class SistemaDeSeguridad{
    - llamables : HashMap<String,Long>
    - dispositivos : List<Dispositivo>
    - estaActivado : boolean
    + armar()
    + desarmar()
    + llamarLlamables()
}

class Alarma{
    + sonar()
}

class Rociador{
    + regar()
}

class Timer{
}

@enduml

```

## Observaciones

- El atributo es minutos pero como ejercicio serán segundos para poder visualizar los cambios
  - En realidad conseguí acelerar el reloj para que cada segundo sean solamente unos milisegundos del mundo real. Queda mejor.
- En Modem, llamarA(...) recibe los datos concretos para no depender de Llamable
- La clase Timer es innecesaria porque el lenguaje escogido provee las herramientas
  - Aún así se ha agregado nuevamente para encapsular el comportamiento, pero se han eliminado las dependencias con otras clases del modelo, ya que el Timer se utilizará como auxiliar para la(s) controladora(s).
- Se agregó en el código un String para identificar a los Dispositivos
- Los llamables serán un Hashmap en vez de una clase. La clase Llamable será desconsiderada.
- Hay implementados dos tipos de observadores.
  - En el modelo, un observador similar al visto en clase.
  - En la vista, se usó un observador basado en las mismas interfaces pero usando lambdas.

## Por hacer o resolver

- Estaria bueno hacer un demonio que cambie las mediciones automáticamente.
- Ponerle labels a los sliders.
- Estoy usando un controladorazo gigante que maneja todo (muchas responsabilidades)
  - Una posibilidad que estoy considerando es que MainController contenga los controladores específicos como atributos.
- Que un dispositivo tenga muchos sensores le agrega complejidad innecesaria (Se hizo un cambio tentativo pero es necesario re-revisarlo).
- CRUDs (<-- Ojo que según consigna es obligatorio por lo menos uno)
  - Llamables
  - Dispositivos (<-- Solamente le falta el modificar)
  - Sensores

De menor importancia:

- Revisar que el diagrama de este README refleje lo que realmente esta en codigo
  - Tal vez merezca un V8 para ese entonces
  - Me preocupa mas sacar andando la aplicacion que el diagrama
- Los modulos de las visatas se separaron en clases y paquetes, pero tal vez necesitan una superclase específica para identificarlos como Modulo, por ejemplo.
- Persistencia. No esta en la consigna pero igual estaría bueno.

## Hecho o resuelto

> - Decidir si un solo timer es suficiente para todo el sistema o es mejor un timer por sensor.
>   - Un timer para todo el sistema: Favorece la presentación.
>   - Muchos timers, uno por sensor: Favorece la calidad del modelo.

👆 Se decidió hacer un solo reloj que maneje todo. Este será parte de la capa de control.

> - En las vistas hay algunas cosas que se pueden abstraer a clases independientes.

👆 Se separaron en clases separadas y se juntaron en un mismo paquete (carpeta)

> - Agregar Dispositivos en las capas view y controller **<-- WIP**

👆 Se creó un formulario en una nueva ventana para crear y unos oportunos botones para eliminarlos. Faltaria modificar pero lo dejo para despues.

## Patrones de diseño

### Patron observer

Es tal vez el patron mas utilizado desde el punto de vista de la codificación para comunicar los eventos.
(Esta previsto poner aquí un pequeño diagramita de qué observa a qué)

### Patron factory method

Dispositivos

### Patron template

Vistas

### Patron strategy

Los dispositivos podrían usar strategy

- Cambiando de sensores
- Convirtiendose en alarma o rociador según algún atributo de tipo programado

### Patrones en la API

No fue programado por mi, pero al usar la API standard de Java los estamos usando.

- Strategy lo usa JPanel y JFrame cuando se le asigna un layout manager.
- Composite en la manera en la que se componen las vistas.
- Decorator en la manera en la que se pueden anidar componentes para cambiar su comportamiento (o su aspecto en el caso de las vistas)
- Abstract Factory al crear bordes para los paneles.
