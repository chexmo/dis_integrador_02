package dis_integrador_02.control;

import dis_integrador_02.model.Dispositivo;
import dis_integrador_02.model.SistemaDeSeguridad;
import dis_integrador_02.shared.Observado;
import dis_integrador_02.shared.Observador;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public class MainController implements Observador, Observado {

    public static ArrayList<DispositivoController> dispositivoControllers = new ArrayList<>();

    private final SistemaDeSeguridad sds;
    private final ArrayList<Observador> observadores = new ArrayList<>();

    public MainController(SistemaDeSeguridad sds) {
        this.sds = sds;
    }

    // Esto queda asi para evitar Singletons
    public static DispositivoController getDispositivoControllerDe(Dispositivo dispositivo) {
        DispositivoController sc = dispositivoControllers
                .stream()
                .filter(c -> c.getDispositivo() == dispositivo)
                .findFirst()
                .orElse(new DispositivoController(dispositivo));
        return sc;
    }

    @Override
    public void agregarObservador(Observador o) {
        this.observadores.add(o);
    }

    @Override
    public void quitarObservador(Observador o) {
        this.observadores.remove(o);
    }

    @Override
    public void notificarObservadores(String mensaje) {
        this.observadores.forEach(o -> o.serNotificado(mensaje));
    }

    @Override
    public void serNotificado(String mensaje) {
        if (mensaje.equals("tick")) {
            this.observadores.forEach(o -> o.serNotificado(mensaje));
        }
        if (mensaje.toLowerCase().startsWith("llamando")) {
            this.mostrarAlerta(mensaje);
        }
    }

    public String getModemDetails() {
        return sds.getModem().getDescripcion();
    }

    public HashMap<String, Long> getLlamables() {
        return sds.getLlamables();
    }

    public String getValorRelojFormateado() {
        return sds.getReloj().getTiempoFormateado();
    }

    public void agregarDispositivoASds(String tipo, String identificador) {
        Dispositivo dispositivo = Dispositivo.crearDispositivo(tipo, identificador);
        sds.agregarDispositivo(dispositivo);
        dispositivoControllers.add(getDispositivoControllerDe(dispositivo));
        notificarObservadores("dispositivo-agregado");
    }

    public void eliminar(DispositivoController dispCont) {
        sds.quitarDispositivo(dispCont.getDispositivo());
        dispositivoControllers.remove(dispCont);
        notificarObservadores("dispositivo-eliminado");
    }

    private void mostrarAlerta(String mensaje) {
        new Thread(
                () -> JOptionPane
                        .showMessageDialog(null, mensaje, "Llamando",
                                JOptionPane.WARNING_MESSAGE))
                .start();
    }

}
