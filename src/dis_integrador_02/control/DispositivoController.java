package dis_integrador_02.control;

import dis_integrador_02.model.Dispositivo;
import lombok.Getter;
import lombok.ToString;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
@ToString
@Getter
public class DispositivoController {

    private final Dispositivo dispositivo;
    private String tipoDisp;
    private int tempEnVista = 15;

    public DispositivoController(Dispositivo dispositivo) {
        this.dispositivo = dispositivo;
        this.averiguarTipo();
    }

    public String getDescripcion() {
        return (tipoDisp + " - " + dispositivo.getIdentificador());
    }

    private void averiguarTipo() {
        this.tipoDisp = this.dispositivo.getClass().getSimpleName();
    }

    public void setUltimaMedicion(int temp) {
        this.tempEnVista = temp;
    }

    public int getTemp() {
        return tempEnVista;
    }
}
