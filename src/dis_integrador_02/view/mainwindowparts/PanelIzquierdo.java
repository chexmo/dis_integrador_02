package dis_integrador_02.view.mainwindowparts;

import dis_integrador_02.control.MainController;
import dis_integrador_02.view.PanelGenerico;
import dis_integrador_02.view.modules.PanelLlamables;
import dis_integrador_02.view.modules.PanelModem;
import java.util.HashMap;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public class PanelIzquierdo extends PanelGenerico {

    private GroupLayout layout;
    private JPanel panelModem;
    private GroupLayout layoutModem;
    private JLabel lbModem;
    private JPanel panelLlamables;
    private JList<String> lsLlamables;
    private GroupLayout layoutLlamables;
    private JTextArea taModem;
    private HashMap<String, Long> llamables;

    public PanelIzquierdo(MainController controller) {
        super(controller);
    }

    @Override
    protected void configurar() {
        this.layout = new GroupLayout(this);
        this.setLayout(this.layout);
    }

    @Override
    protected void agregarComponentes() {
        this.panelModem = this.crearPanelModem();
        this.add(this.panelModem);

        this.panelLlamables = this.crearPanelLlamables();
        this.add(this.panelLlamables);
    }

    @Override
    protected void maquetar() {
        // No crear variables en este metodo.
        layout.setHorizontalGroup(
                layout.createParallelGroup()
                        .addComponent(panelModem, GroupLayout.Alignment.LEADING, 220, 220, 300)
                        .addComponent(panelLlamables, GroupLayout.Alignment.LEADING, 220, 220, 300)
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addComponent(panelModem, 30, 30, 70)
                        .addComponent(panelLlamables)
        );
    }

    private JPanel crearPanelModem() {
        PanelModem p = new PanelModem(controller);
        p.iniciar();
        return p;
    }

    private JPanel crearPanelLlamables() {
        PanelLlamables p = new PanelLlamables(controller);
        p.iniciar();
        return p;
    }

}
