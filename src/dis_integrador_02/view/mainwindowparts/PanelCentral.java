package dis_integrador_02.view.mainwindowparts;

import dis_integrador_02.control.MainController;
import dis_integrador_02.view.PanelGenerico;
import dis_integrador_02.view.modules.PanelDispositivos;
import dis_integrador_02.view.modules.PanelReloj;
import java.awt.Dimension;
import javax.swing.GroupLayout;
import javax.swing.JPanel;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public class PanelCentral extends PanelGenerico {

    private GroupLayout layout;
    private JPanel panelReloj;
    private JPanel panelDispositivos;

    public PanelCentral(MainController controller) {
        super(controller);
    }

    @Override
    protected void configurar() {
        this.layout = new GroupLayout(this);
        this.setLayout(this.layout);
        this.setMinimumSize(new Dimension(250, 500));
    }

    @Override
    protected void agregarComponentes() {
        this.panelReloj = this.crearPanelReloj();
        this.add(this.panelReloj);

        this.panelDispositivos = this.crearPanelDispositivos();
        this.add(this.panelDispositivos);
    }

    @Override
    protected void maquetar() {
        // No crear variables en este metodo.
        layout.setHorizontalGroup(
                layout.createParallelGroup()
                        .addComponent(panelReloj, 100, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(panelDispositivos, 100, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addComponent(panelReloj)
                        .addComponent(panelDispositivos, 100, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }

    private JPanel crearPanelReloj() {
        PanelReloj jp = new PanelReloj(controller);
        jp.iniciar();
        return jp;
    }

    private JPanel crearPanelDispositivos() {
        PanelDispositivos pd = new PanelDispositivos(controller);
        pd.iniciar();
        return pd;
    }

}
