package dis_integrador_02.view;

import dis_integrador_02.control.MainController;
import java.awt.Dimension;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public class FormularioDispositivo extends JFrame {

    private final MainController controller;
    private GroupLayout layout;
    private JPanel radsTipoDisp;
    private ButtonGroup buttonGroup;
    private JRadioButton btnAlarmaTermica;
    private JRadioButton btnRociadorTermico;
    private String tipoElegido;
    private JPanel panelPrincipal;
    private JPanel textboxIdentificador;
    private JTextField tfIdentificador;
    private JButton btnAceptar;
    private JButton btnCancelar;

    public FormularioDispositivo(MainController controller) {
        this.controller = controller;
    }

    public void iniciar() {
        this.configurar();
        this.agregarComponentes();
        this.maquetar();
    }

    private void configurar() {
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setMinimumSize(new Dimension(400, 250));
        this.panelPrincipal = new JPanel();
        this.layout = new GroupLayout(panelPrincipal);
        this.panelPrincipal.setLayout(this.layout);
        this.add(panelPrincipal);
    }

    private void agregarComponentes() {
        this.agregarRadsTipoDisp();
        this.agregarTextboxIdentificador();
        this.agregarBotones();
    }

    private void maquetar() {
        layout.setHorizontalGroup(layout.createParallelGroup()
                .addComponent(radsTipoDisp)
                .addComponent(textboxIdentificador)
                .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnAceptar)
                        .addGap(5)
                        .addComponent(btnCancelar)
                        .addGap(5)
                )
        );
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGap(5)
                .addComponent(radsTipoDisp)
                .addGap(5)
                .addComponent(textboxIdentificador)
                .addGap(5)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addComponent(btnAceptar)
                        .addComponent(btnCancelar)
                )
                .addGap(5)
        );
    }

    private void agregarRadsTipoDisp() {
        this.radsTipoDisp = new JPanel();
        PanelGenerico.decorarPanel(radsTipoDisp, "Elegir tipo de dispositivo");

        this.buttonGroup = new ButtonGroup();

        this.btnAlarmaTermica = new JRadioButton("Alarma Termica");
        this.buttonGroup.add(btnAlarmaTermica);
        this.btnAlarmaTermica.addChangeListener(l -> tipoElegido = "alarmatermica");
        radsTipoDisp.add(btnAlarmaTermica);

        this.btnRociadorTermico = new JRadioButton("Rociador Termico");
        this.buttonGroup.add(btnRociadorTermico);
        this.btnRociadorTermico.addChangeListener(l -> tipoElegido = "rociadortermico");
        radsTipoDisp.add(btnRociadorTermico);

        this.panelPrincipal.add(radsTipoDisp);
    }

    private void agregarTextboxIdentificador() {
        this.textboxIdentificador = new JPanel();
        PanelGenerico.decorarPanel(textboxIdentificador, "Nombre del dispositivo");

        this.tfIdentificador = new JTextField(30);
        textboxIdentificador.add(tfIdentificador);

        this.panelPrincipal.add(textboxIdentificador);
    }

    private void agregarBotones() {
        this.btnAceptar = new JButton("Aceptar");
        btnAceptar.addActionListener(e -> aceptar());

        this.btnCancelar = new JButton("Cancelar");
        btnCancelar.addActionListener(e -> cancelar());
    }

    private void aceptar() {
        String tipo = tipoElegido;
        String identificador = tfIdentificador.getText();
        controller.agregarDispositivoASds(tipo, identificador);
        try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {
            Logger.getLogger(FormularioDispositivo.class.getName()).log(Level.SEVERE, null, ex);
        }
        dispose();
    }

    private void cancelar() {
        this.dispose();
    }

}
