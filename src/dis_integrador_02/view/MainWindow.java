package dis_integrador_02.view;

import dis_integrador_02.control.MainController;
import dis_integrador_02.view.mainwindowparts.PanelCentral;
import dis_integrador_02.view.mainwindowparts.PanelIzquierdo;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MainWindow extends JFrame {

    private MainController controller;
    private JPanel panelGeneral;
    private JPanel panelIzquierdo;
    private BorderLayout layout;
    private JLabel lbTitulo;
    private JPanel panelCentral;

    public MainWindow(MainController controller) {
        this.controller = controller;
    }

    public void iniciar() {
        this.configurar();
        this.agregarComponentes();

        this.validate();
    }

    private void configurar() {
        this.setTitle("Chiquisoft security pack");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        this.panelGeneral = new JPanel();
        this.layout = new BorderLayout(10, 10);
        this.panelGeneral.setLayout(layout);
        this.panelGeneral.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.add(panelGeneral);

        this.setMinimumSize(new Dimension(700, 500));
    }

    private void agregarComponentes() {
        this.lbTitulo = this.crearTitulo();
        this.panelGeneral.add(this.lbTitulo, BorderLayout.NORTH);

        this.panelIzquierdo = this.crearPanelIzquierdo();
        this.panelGeneral.add(this.panelIzquierdo, BorderLayout.WEST);

        this.panelCentral = this.crearPanelCentral();
        this.panelGeneral.add(this.panelCentral, BorderLayout.CENTER);
    }

    private JLabel crearTitulo() {
        JLabel jLabel = new JLabel("    Gestor de alarmas ISFT194    ");
        jLabel.setFont(
                new Font(
                        jLabel.getFont().getFontName(),
                        jLabel.getFont().getStyle(),
                        40)
        );
        jLabel.setHorizontalAlignment(JLabel.CENTER);
        return jLabel;
    }

    private JPanel crearPanelIzquierdo() {
        PanelIzquierdo jp = new PanelIzquierdo(controller);
        jp.iniciar();
        return jp;
    }

    private JPanel crearPanelCentral() {
        PanelCentral jp = new PanelCentral(controller);
        jp.iniciar();
        return jp;
    }

}
