package dis_integrador_02.view;

import dis_integrador_02.control.MainController;
import java.util.Objects;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public abstract class PanelGenerico extends JPanel {

    protected final MainController controller;

    public PanelGenerico(MainController controller) {
        this.controller = controller;
    }

    public void iniciar() {  // Template pattern
        this.configurar();
        this.agregarComponentes();
        this.maquetar();
    }

    protected abstract void configurar();

    protected abstract void agregarComponentes();

    protected abstract void maquetar();

    protected static void decorarPanel(JPanel panel, String titulo) {
        Border borde = BorderFactory.createEtchedBorder();
        Border margen = BorderFactory.createEmptyBorder(5, 5, 5, 5);
        Border padding = BorderFactory.createEmptyBorder(5, 5, 5, 5);

        Border bfinal;
        if (Objects.isNull(titulo) || titulo.isEmpty()) {
            Border compuesto = BorderFactory.createCompoundBorder(margen, borde);
            bfinal = BorderFactory.createCompoundBorder(compuesto, padding);
        } else {
            Border titulado = BorderFactory.createTitledBorder(borde, titulo);
            Border compuesto = BorderFactory.createCompoundBorder(margen, titulado);
            bfinal = BorderFactory.createCompoundBorder(compuesto, padding);
        }

        panel.setBorder(bfinal);
    }

}
