package dis_integrador_02.view.modules;

import dis_integrador_02.control.DispositivoController;
import dis_integrador_02.control.MainController;
import dis_integrador_02.view.FormularioDispositivo;
import dis_integrador_02.view.PanelGenerico;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public class PanelDispositivos extends PanelGenerico {

    private GroupLayout layout;
    private JButton btnAgregar;
    private JPanel panelDeDispositivos;
    private BoxLayout layoutDeDispositivos;
    private JScrollPane wrapperDispositivos;
    private List<PanelDispositivoIndividual> panelesDispIndiv = new ArrayList<>();

    public PanelDispositivos(MainController controller) {
        super(controller);
    }

    @Override
    protected void configurar() {
        this.layout = new GroupLayout(this);
        this.setLayout(this.layout);
        PanelLlamables.decorarPanel(this, "Dispositivos conectados");
        this.setMinimumSize(new Dimension(200, 100));

        this.observarMainController();
    }

    @Override
    protected void agregarComponentes() {
        this.btnAgregar = this.crearBotonAgregar();
        this.add(this.btnAgregar);

        this.agregarPanelDispositivos();
        this.refrescarDispositivos();
    }

    @Override
    protected void maquetar() {
        layout.setHorizontalGroup(
                layout.createParallelGroup()
                        .addComponent(btnAgregar)
                        .addComponent(wrapperDispositivos)
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addComponent(btnAgregar)
                        .addGap(5)
                        .addComponent(wrapperDispositivos)
        );
    }

    private void observarMainController() {
        controller.agregarObservador(
                m -> {
                    if (m.contains("dispositivo-agregado")
                            || m.contains("dispositivo-eliminado")) {
                        this.refrescarDispositivos();
                    }
                }
        );
    }

    private JButton crearBotonAgregar() {
        JButton jb = new JButton("Agregar");
        jb.addActionListener(l -> mostrarFormAgregarDisp());
        return jb;
    }

    private void agregarPanelDispositivos() {
        this.panelDeDispositivos = new JPanel();
        this.layoutDeDispositivos = new BoxLayout(this.panelDeDispositivos, BoxLayout.Y_AXIS);
        this.panelDeDispositivos.setLayout(layoutDeDispositivos);
        this.wrapperDispositivos = new JScrollPane(this.panelDeDispositivos);
        this.add(this.wrapperDispositivos);
    }

    private void refrescarDispositivos() {
        // FIXME esto esta horrible pero el tiempo corre
        panelDeDispositivos.removeAll();
        panelesDispIndiv.clear();
        MainController.dispositivoControllers
                .forEach(c -> agregarDispositivoIndividual(c));
        panelesDispIndiv.forEach(p -> panelDeDispositivos.add(p));
        panelDeDispositivos.revalidate();
        panelDeDispositivos.repaint();
    }

    private void agregarDispositivoIndividual(DispositivoController dispCont) {
        PanelDispositivoIndividual pdi = new PanelDispositivoIndividual(controller, dispCont);
        pdi.iniciar();
        this.panelesDispIndiv.add(pdi);
    }

    private void mostrarFormAgregarDisp() {
        FormularioDispositivo fd = new FormularioDispositivo(controller);
        fd.iniciar();
        fd.setVisible(true);
    }

}
