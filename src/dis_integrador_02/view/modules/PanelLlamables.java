package dis_integrador_02.view.modules;

import dis_integrador_02.control.MainController;
import dis_integrador_02.view.PanelGenerico;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.GroupLayout;
import javax.swing.JList;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public class PanelLlamables extends PanelGenerico {

    private HashMap<String, Long> llamables;
    private JList<String> lsLlamables;
    private GroupLayout layout;

    public PanelLlamables(MainController controller) {
        super(controller);
    }

    @Override
    protected void configurar() {
        this.layout = new GroupLayout(this);
        this.setLayout(this.layout);
        PanelLlamables.decorarPanel(this, "Llamar a");
    }

    @Override
    protected void agregarComponentes() {
        this.llamables = controller.getLlamables();
        ArrayList<String> str = new ArrayList<>();
        this.llamables.forEach((k, v) -> str.add(k + ":  " + v));
        this.lsLlamables = new JList<>(str.toArray(new String[1]));
    }

    @Override
    protected void maquetar() {
        layout.setHorizontalGroup(
                layout.createParallelGroup()
                        .addComponent(this.lsLlamables, 100, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addComponent(this.lsLlamables)
        );
    }

}
