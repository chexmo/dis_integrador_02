package dis_integrador_02.view.modules;

import dis_integrador_02.control.MainController;
import dis_integrador_02.view.PanelGenerico;
import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public class PanelReloj extends PanelGenerico {
    
    private BoxLayout layout;
    private JLabel lbTiempo;
    
    public PanelReloj(MainController controller) {
        super(controller);
    }
    
    @Override
    protected void configurar() {
        this.layout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(layout);
        PanelReloj.decorarPanel(this, "Reloj");
    }
    
    @Override
    protected void agregarComponentes() {
        this.lbTiempo = new JLabel("Cargando...");
        this.lbTiempo.setFont(
                new Font(
                        this.lbTiempo.getFont().getName(),
                        this.lbTiempo.getFont().getStyle(),
                        24
                )
        );
        controller.agregarObservador((String mensaje) -> {
            if (mensaje.contains("tick")) {
                lbTiempo.setText(controller.getValorRelojFormateado());
            }
        });
        this.lbTiempo.setHorizontalAlignment(JLabel.CENTER);
        this.lbTiempo.setAlignmentX(CENTER_ALIGNMENT);
        this.add(this.lbTiempo);
    }
    
    @Override
    protected void maquetar() {
    }
    
    private void tick(String s) {
        this.lbTiempo.setText(s);
    }
    
}
