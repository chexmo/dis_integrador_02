package dis_integrador_02.view.modules;

import dis_integrador_02.control.MainController;
import dis_integrador_02.view.PanelGenerico;
import javax.swing.GroupLayout;
import javax.swing.JTextArea;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public class PanelModem extends PanelGenerico {

    private JTextArea taModem;
    private GroupLayout layout;

    public PanelModem(MainController controller) {
        super(controller);
    }

    @Override
    protected void configurar() {
        this.layout = new GroupLayout(this);
        this.setLayout(this.layout);
        PanelModem.decorarPanel(this, "Info del modem");
    }

    @Override
    protected void agregarComponentes() {
        this.taModem = new JTextArea(controller.getModemDetails());
        this.taModem.setWrapStyleWord(true);
        this.taModem.setLineWrap(true);
        this.taModem.setOpaque(false);
        this.taModem.setEditable(false);
        this.taModem.setFocusable(false);
    }

    @Override
    protected void maquetar() {
        layout.setHorizontalGroup(
                layout.createParallelGroup()
                        .addComponent(taModem)
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addComponent(taModem)
        );
    }

}
