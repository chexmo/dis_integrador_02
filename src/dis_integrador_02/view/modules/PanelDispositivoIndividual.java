/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dis_integrador_02.view.modules;

import dis_integrador_02.control.DispositivoController;
import dis_integrador_02.control.MainController;
import dis_integrador_02.view.PanelGenerico;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSlider;
import lombok.Getter;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
@Getter
public class PanelDispositivoIndividual extends PanelGenerico {

    private final DispositivoController dispCont;
    private JLabel lbDispositivo;
    private GroupLayout layout;
    private JButton btnEliminar;
    private JLabel lbIdentificador;
    private JSlider sldTemp;

    public PanelDispositivoIndividual(MainController controller, DispositivoController dispositivoController) {
        super(controller);
        this.dispCont = dispositivoController;
    }

    @Override
    protected void configurar() {
        this.layout = new GroupLayout(this);
        this.setLayout(layout);
        PanelReloj.decorarPanel(this, null);
    }

    @Override
    protected void agregarComponentes() {
        this.lbDispositivo = new JLabel(dispCont.getTipoDisp());
        this.add(this.lbDispositivo);

        this.lbIdentificador = new JLabel(dispCont.getDispositivo().getIdentificador());
        this.add(this.lbIdentificador);

        this.sldTemp = this.crearSliderTemp();
        this.add(this.sldTemp);

        this.btnEliminar = this.crearBotonEliminar();
        this.add(this.btnEliminar);
    }

    @Override
    protected void maquetar() {
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                        .addComponent(lbDispositivo, 100, 100, 200)
                        .addComponent(lbIdentificador, 100, 100, 200)
                )
                .addComponent(sldTemp, 50, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                .addComponent(btnEliminar)
        );
        layout.setVerticalGroup(layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                        .addComponent(this.lbDispositivo)
                        .addComponent(this.lbIdentificador)
                )
                .addComponent(sldTemp)
                .addComponent(btnEliminar)
        );
    }

    private JSlider crearSliderTemp() {
        JSlider js = new JSlider(-10, 100, 10);
        js.addChangeListener(
                e -> {
                    JSlider source = (JSlider) e.getSource();
                    if (!source.getValueIsAdjusting()) {
                        int temp = (int) source.getValue();
                        dispCont.setUltimaMedicion(temp);
                    }
                }
        );
        return js;
    }

    private JButton crearBotonEliminar() {
        JButton jb = new JButton("Eliminar");
        jb.addActionListener(l -> controller.eliminar(dispCont));
        return jb;
    }

}
