package dis_integrador_02.shared;

public interface Observado {

    public void agregarObservador(Observador o);

    public void quitarObservador(Observador o);

    public void notificarObservadores(String mensaje);

}
