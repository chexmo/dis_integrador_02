package dis_integrador_02.shared;

public interface Observador {

    public void serNotificado(String mensaje);

}
