package dis_integrador_02.model;

import dis_integrador_02.shared.Observado;
import dis_integrador_02.shared.Observador;
import java.util.ArrayList;
import lombok.Data;

@Data
public class Modem implements Observado {

    private final int puerto;
    private ArrayList<Observador> observadores = new ArrayList<Observador>();

    public void llamarA(long numero, String mensaje) {
        notificarObservadores("Llamando a " + numero + " para entregar el mensaje: " + mensaje);
    }

    public String getDescripcion() {
        return "Modem generico - Puerto: " + puerto;
    }

    @Override
    public void agregarObservador(Observador o) {
        this.observadores.add(o);
    }

    @Override
    public void quitarObservador(Observador o) {
        this.observadores.remove(o);
    }

    @Override
    public void notificarObservadores(String mensaje) {
        observadores.forEach(o -> o.serNotificado(mensaje));
    }

}
