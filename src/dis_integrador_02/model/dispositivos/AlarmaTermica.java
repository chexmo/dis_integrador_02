package dis_integrador_02.model.dispositivos;

import dis_integrador_02.model.Dispositivo;
import dis_integrador_02.model.Sensor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AlarmaTermica extends Dispositivo {

    public AlarmaTermica(String identificador) {
        super(identificador);
        this.umbral = 60;
        this.sensor = new Sensor(this);
    }

    @Override
    public void activarse() {
        sonar();
        notificarObservadores("Alarma sonando! peligro de incendio");
    }

    private void sonar() {
        notificarObservadores("alarma termica sonando");
    }

}
