package dis_integrador_02.model.dispositivos;

import dis_integrador_02.model.Dispositivo;
import dis_integrador_02.model.Sensor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RociadorTermico extends Dispositivo {

    public RociadorTermico(String identificador) {
        super(identificador);
        umbral = 65;
        sensor = new Sensor(this);
    }

    @Override
    public void activarse() {
        regar();
        notificarObservadores("rociador termico activado incendio");
    }

    private void regar() {
        notificarObservadores("rociador termico tirando agua");
    }
}
