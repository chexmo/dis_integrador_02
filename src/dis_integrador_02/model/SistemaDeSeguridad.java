package dis_integrador_02.model;

import dis_integrador_02.shared.Observado;
import dis_integrador_02.shared.Observador;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lombok.Data;

@Data
public final class SistemaDeSeguridad implements Observador, Observado {

    private ArrayList<Observador> observadores = new ArrayList<Observador>();
    private HashMap<String, Long> llamables = new HashMap<>();
    private List<Dispositivo> dispositivos = new ArrayList<>();
    private Modem modem;
    private Reloj reloj;

    public SistemaDeSeguridad(HashMap<String, Long> llamables, Modem modem, Reloj reloj) {
        this.llamables = llamables;
        this.modem = modem;
        this.reloj = reloj;
    }

    @Override
    public void serNotificado(String mensaje) {
        if (mensaje.toLowerCase().contains("incendio")) {
            llamables.forEach((k, v) -> modem.llamarA(v, "Peligro de incendio"));
        }
        if (mensaje.toLowerCase().contains("tick")) {
            observadores.forEach(o -> o.serNotificado(mensaje));
        }
    }

    @Override
    public void agregarObservador(Observador o) {
        this.observadores.add(o);
    }

    @Override
    public void quitarObservador(Observador o) {
        this.observadores.remove(o);
    }

    @Override
    public void notificarObservadores(String mensaje) {
        this.observadores.forEach(o -> o.serNotificado(mensaje));
    }

    public void agregarDispositivo(Dispositivo dispositivo) {
        dispositivo.agregarObservador(this);
        reloj.agregarObservador(dispositivo.getSensor());
        dispositivos.add(dispositivo);
    }

    public void quitarDispositivo(Dispositivo dispositivo) {
        dispositivo.quitarObservador(this);
        dispositivos.remove(dispositivo);
    }
}
