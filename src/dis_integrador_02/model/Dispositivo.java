package dis_integrador_02.model;

import dis_integrador_02.model.dispositivos.AlarmaTermica;
import dis_integrador_02.model.dispositivos.RociadorTermico;
import dis_integrador_02.shared.Observado;
import dis_integrador_02.shared.Observador;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public abstract class Dispositivo implements Observado {

    protected List<Observador> observadores = new ArrayList<>();
    protected Sensor sensor;
    protected final String identificador;
    protected int umbral = 0;

    public Dispositivo(String identificador) {
        this.identificador = identificador;
    }

    public void actualizar() {
        if (umbral < sensor.getUltimaMedicion()) {
            activarse();
        }
    }

    public abstract void activarse();

    @Override
    public void agregarObservador(Observador o) {
        this.observadores.add(o);
    }

    @Override
    public void quitarObservador(Observador o) {
        this.observadores.remove(o);
    }

    @Override
    public void notificarObservadores(String mensaje) {
        this.observadores.forEach(o -> o.serNotificado(mensaje));
    }

    // Kind-of factory method
    public static Dispositivo crearDispositivo(String tipo, String identificador) {
        Dispositivo dispositivo = null;
        if (tipo.toLowerCase().contains("alarmatermica")) {
            dispositivo = new AlarmaTermica(identificador);
        } else if (tipo.toLowerCase().contains("rociadortermico")) {
            dispositivo = new RociadorTermico(identificador);
        }
        return dispositivo;
    }
}
