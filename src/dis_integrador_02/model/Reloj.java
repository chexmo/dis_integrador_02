package dis_integrador_02.model;

import dis_integrador_02.shared.Observado;
import dis_integrador_02.shared.Observador;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import lombok.Getter;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
@Getter
public class Reloj implements Observado {

    private ArrayList<Observador> observadores = new ArrayList<>();

    private int hh = 0;
    private int mm = 0;
    private int ss = 0;

    private final Timer timer;
    private TimerTask task = new TimerTask() {
        @Override
        public void run() {
            tick();
        }
    };

    public Reloj(long delay, long duracionDelTick) {
        this.timer = new Timer();
        this.timer.scheduleAtFixedRate(this.task, delay, duracionDelTick);
    }

    @Override
    public void agregarObservador(Observador o) {
        this.observadores.add(o);
    }

    @Override
    public void quitarObservador(Observador o) {
        this.observadores.remove(o);
    }

    @Override
    public void notificarObservadores(String mensaje) {
        this.observadores.forEach(o -> o.serNotificado(mensaje));
    }

    private void tick() {
        ss++;

        if (ss >= 60) {
            this.notificarObservadores("minuto");
            mm++;
            ss = 0;

            if (mm >= 60) {
                hh++;
                mm = 0;

                if (hh >= 24) {
                    hh = 0;
                }
            }
        }
        this.notificarObservadores("tick");
    }

    public String getTiempoFormateado() {
        StringBuilder sb = new StringBuilder();
        sb.append(" ");
        sb.append(String.format("%02d", hh));
        sb.append(" : ");
        sb.append(String.format("%02d", mm));
        sb.append(" : ");
        sb.append(String.format("%02d", ss));
        sb.append(" ");
        return sb.toString();
    }
}
