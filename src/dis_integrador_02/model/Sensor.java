package dis_integrador_02.model;

import dis_integrador_02.control.MainController;
import dis_integrador_02.shared.Observador;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class Sensor implements Observador {

    protected List<Observador> observadores = new ArrayList<>();
    protected int ultimaMedicion = Integer.MIN_VALUE;
    protected Dispositivo dispositivo;

    public Sensor(Dispositivo dispositivo) { // <-- Dependency incjection
        this.dispositivo = dispositivo;
    }

    @Override
    public void serNotificado(String mensaje) {
        if (mensaje.contains("minuto")) {
            this.medir();
        }
    }

    public void medir() {
        ultimaMedicion = MainController.getDispositivoControllerDe(dispositivo).getTemp();
        System.out.println("ultimamedicion:" + ultimaMedicion);
        dispositivo.actualizar();
    }

}
