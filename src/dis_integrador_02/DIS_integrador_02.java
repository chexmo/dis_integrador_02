package dis_integrador_02;

import com.formdev.flatlaf.FlatDarkLaf;
import dis_integrador_02.control.MainController;
import dis_integrador_02.model.Modem;
import dis_integrador_02.model.Reloj;
import dis_integrador_02.model.SistemaDeSeguridad;
import dis_integrador_02.view.MainWindow;
import java.util.HashMap;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public class DIS_integrador_02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(new FlatDarkLaf());
        } catch (UnsupportedLookAndFeelException ex) {
        }

        // TO_DO hacer CRUDs o archivos de configuracion para no tener que hacer esto
        Modem modem = new Modem(5403);
        HashMap<String, Long> llamables = new HashMap<>();
        llamables.put("Emergencias", 4550050l);
        llamables.put("Bomberos", 4249949l);
        llamables.put("Policia", 4029939l);
        // fin del to-do

        Reloj reloj = new Reloj(3000, 200);
        SistemaDeSeguridad sds = new SistemaDeSeguridad(llamables, modem, reloj);
        reloj.agregarObservador(sds);

        MainController controller = new MainController(sds);
        sds.agregarObservador(controller);
        sds.getModem().agregarObservador(controller);

        MainWindow window = new MainWindow(controller);
        window.iniciar();
        window.setVisible(true);
    }

}
